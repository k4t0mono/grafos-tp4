#!/usr/bin/env python3
# coding=utf-8

from math import sqrt
from copy import deepcopy
import sys

class Sudoku:
    def __init__(self, arq:str):
        self.direcionado = False
        fl = open(arq, "r")
        lines = fl.read().splitlines()
        self.nLinhas = len(lines)
        self.quadrosPL = int(sqrt(self.nLinhas))
        self.nVertices = self.nLinhas*self.nLinhas

        # Criar os vertice
        self.vertices = []
        count = 0
        for i in range(self.nLinhas):
            self.vertices.append([])
            for j in range(self.nLinhas):
                v = {"tag":"<{},{}>".format(i, j), "cor":0, "i":i, "j":j, "k":count}
                self.vertices[i].append(v)
                count += 1

        # Colocar as cores
        for i in range(self.nLinhas):
            s = lines[i].split()
            for j in range(self.nLinhas):
                if(s[j] != '.'):
                    cor = int(s[j])
                    self.vertices[i][j]["cor"] = cor

        # Ligar vertices da msm linha
        self.arestas = []
        # acessar cada linha
        for i in range(self.nLinhas):
            # acessar cada vertice
            for j in range(self.nLinhas):
                # acessar os proximos
                for k in range(j+1, self.nLinhas):
                    a = ((i, j), (i, k))
                    self.arestas.append(a)
        
        # Ligar vertices da msm coluna
        for j in range(self.nLinhas):
            for i in range(self.nLinhas):
                for k in range(i+1, self.nLinhas):
                    a = ((i, j), (k, j))
                    self.arestas.append(a)
        
        # Ligar vertices do msm quadrado
        q = []
        for x in range(0, self.nLinhas, self.quadrosPL):
            for y in range(0, self.nLinhas, self.quadrosPL):
                q.append([])
                for i in range(x, x+self.quadrosPL):
                    for j in range(y, y+self.quadrosPL):
                        q[-1].append((i, j))
        
        # Criar as arestas do msm quadrado
        for i in range(len(q)):
            for j in range(len(q[i])):
                for k in range(j+1, len(q[i])):
                    a = (q[i][j], q[i][k])
                    self.arestas.append(a)

        self.arestas = list(set(self.arestas))
        self.arestas.sort()

        self.gerarMA()

    # =========================================================================

    def gerarMA(self):
        """Gera a Matriz de adjacencia do grafo"""

        self.estrutura = []
        for i in range(self.nVertices):
            self.estrutura.append([])
            for j in range(self.nVertices):
                self.estrutura[i].append(0)

        for i in self.arestas:
            a = self.vertices[i[0][0]][i[0][1]]["k"]
            b = self.vertices[i[1][0]][i[1][1]]["k"]
            self.estrutura[a][b] = 1
            self.estrutura[b][a] = 1

    # =========================================================================

    def vizinhos(self, *args):
        """Retorna a lista de vizinhos de v"""

        if(len(args) == 1):
            v = args[0]
        elif(len(args) == 2):
            v = self.vertices[args[0]][args[1]]["k"]
        else:
            raise "ERROR: Grafo.vizinhos"

        vizinhos = []

        for i in range(self.nVertices):
            if(self.estrutura[v][i]):
                # achar a linha do vertice
                _l = i//self.nLinhas
                # achar a coluna do vertice
                _c = i%self.nLinhas
                _u = self.vertices[_l][_c]
                vizinhos.append(_u)

        return vizinhos

    # ========================================================================

    def grau(self, *args):
        if(len(args) == 1):
            v = args[0]
        elif(len(args) == 2):
            v = self.vertices[args[0]][args[1]]["k"]
        else:
            raise "ERROR: Grafo.grau"

        return len(self.vizinhos(v))

    # ========================================================================

    def grauSaturacao(self, *args):
        if(len(args) == 1):
            v = args[0]
        elif(len(args) == 2):
            v = self.vertices[args[0]][args[1]]["k"]
        else:
            raise "ERROR: Grafo.grauSaturacao"

        vizinhos = self.vizinhos(v)
        cores = list(range(1,self.nLinhas+1))
        for i in vizinhos:
            if(i["cor"] in cores):
                cores.remove(i["cor"])

        return self.nLinhas-len(cores)

    # ========================================================================

    def sudokuCompleto(self):
        for i in range(self.nLinhas):
            for j in range(self.nLinhas):
                if(self.vertices[i][j]["cor"] == 0):
                    return False

        return True

    # ========================================================================

    def dsaturBT(self):
        # Criar lista de vértices válidos
        verticesValidos = []
        for i in self.vertices:
            for j in i:
                if(j["cor"] > 0):
                    continue

                v = j
                verticesValidos.append(v)

        r = self.dsaturBTAux(verticesValidos)
        
        if(not r):
            raise "ERROR: Sudoku insolúvel"
            return False
        else:
            return r

    def dsaturBTAux(self, verticesValidos):
        # Verifica se o sudoku está completo
        if(self.sudokuCompleto()):
            return True

        # Enquanto houver vértices válidos
        while(len(verticesValidos)):
            # Calcular grau de saturação
            for i in range(len(verticesValidos)):
                verticesValidos[i]["grauS"] = self.grauSaturacao(verticesValidos[i]["k"])

            # Ordenar vértices pelo grau de saturação
            verticesValidos.sort(key=lambda k: k['grauS'], reverse=True)
            
            # Pegar o vértice com maior grau de saturação
            v = verticesValidos.pop(0)

            # Calcular cores disponiveis
            vizinhos = self.vizinhos(v["k"])
            cores = list(range(1,self.nLinhas+1))
            for i in vizinhos:
                if(i["cor"] in cores):
                    cores.remove(i["cor"])
            
            # Para cada cor disponível
            for i in cores:
                v["cor"] = i
                # Continuar o algoritimo
                r = self.dsaturBTAux(verticesValidos)
                
                # Se o sudoku estiver completo
                if(r):
                    # Sair do algorimo com True
                    return True
            
            # Se passar por todas as cores
            # Zerar a cor do vértice
            v["cor"] = 0
            # Recolocar na lista
            verticesValidos.append(v)
            # Retornar falso            
            return False


    # ========================================================================


    def __str__(self):
        a = ""
        for i in range(self.nLinhas):
            for j in range(self.nLinhas):
                if(self.vertices[i][j]["cor"] == 0):
                    a += "."
                else:
                    a += "{} ".format(self.vertices[i][j]["cor"], end=" ")
            a += "\n"
        
        return a.rstrip()
