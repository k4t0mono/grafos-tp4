#!/usr/bin/env python3
# coding=utf-8

from collections import deque

class Arvore:
    def __init__(self, arq:str):
        # Abre o arquivo
        fl = open(arq, "r")
        # Ler o arquivo
        lines = fl.read().splitlines()

        # Armazenar as caracteristicas do grafo
        self.direcionado = True if lines[1] == "DIRECTED" else False
        self.valorado = True if lines[2] == "WEIGHTED" else False
        self.nVertices = int(lines[3])

        # Arestas
        self.arestas = []
        # Ler as linhas com as arestas
        for i in range(4, len(lines)):
            # Ler os vertices
            tmp = lines[i].split()
            tmp[0] = int(tmp[0])
            tmp[1] = int(tmp[1])
            
            # Criar as arestas
            a = {"u":tmp[0], "v":tmp[1], "cor":0, "peso":1}
            
            # ser for valorado, colocar a peso
            if self.valorado:
                a["peso"] = int(tmp[2])
            
            self.arestas.append(a)
        
        self.nAresta = len(self.arestas)
        
        self.gerarMA()
        
    # =========================================================================
    
    def gerarMA(self):
        self.estrutura = []
        for i in range(0, self.nVertices):
            self.estrutura.append([])
            for j in range(0, self.nVertices):
                self.estrutura[i].append(0)
                
        for i in self.arestas:
            self.estrutura[i["u"]][i["v"]] = i["peso"]
            if(not self.direcionado):
                self.estrutura[i["v"]][i["u"]] = i["peso"]
                
    # =========================================================================
    
    def __str__(self):
        
